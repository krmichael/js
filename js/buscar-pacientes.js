"use strict";

document.querySelector('#buscar-pacientes').addEventListener('click', () => {
  
  const XHR = new XMLHttpRequest(),
        url = "https://api-pacientes.herokuapp.com/pacientes",
        erro = document.querySelector('#erro-ajax');

  XHR.open("GET", url);
  
  XHR.readyState === 4 && XHR.status === 200 ? (
    XHR.addEventListener('load', () => {

      erro.classList.add('hide');
      const RESPOSTA = XHR.responseText,
            PACIENTES = JSON.parse(RESPOSTA);
      
      PACIENTES.forEach(paciente => {
        adicionaPacienteNatabela(paciente);
      })

    })
  ) : (
    console.log(`Status: ${XHR.status}\nErro: ${XHR.responseText}`),

    erro.classList.remove('hide'),

    setTimeout(() => { erro.classList.add('hide') }, 4000)
  )

  XHR.send();
});