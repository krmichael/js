
"use strict";

const PACIENTES = document.querySelectorAll('.paciente');

const VALIDA_PESO = peso => peso > 0 && peso < 500 ? true : false;
const VALIDA_ALTURA = altura => altura > 0 && altura < 3 ? true : false;

const CALCULA_IMC = (peso, altura) => {
	let imc = 0;
	imc = peso / (altura * altura);
	return imc.toFixed(2);
}

PACIENTES.forEach(paciente => {

	let peso = paciente.querySelector('.peso').textContent,
			altura = paciente.querySelector('.altura').textContent,
			imc = paciente.querySelector('.imc'),
			pesoValido = VALIDA_PESO(peso),
			alturaValida = VALIDA_ALTURA(altura);

	!pesoValido ? (
		imc.textContent = "Peso inválido!",
		paciente.classList.add('errors')
	) :
	!alturaValida ? (
		imc.textContent = "Altura inválida!",
		paciente.classList.add('errors')
	) : (
		imc.textContent = CALCULA_IMC(peso, altura)
	)

});