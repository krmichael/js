"use strict";

document.querySelector('#filtrar-paciente').addEventListener('input', (e) => {
  
  const PACIENTES = document.querySelectorAll('.paciente');

  e.target.value !== "" ? (
    PACIENTES.forEach(paciente => {

      let nome = paciente.querySelector('.nome').textContent,
          expressao = new RegExp(e.target.value, "i");

      !expressao.test(nome) ? paciente.classList.add('hide') : paciente.classList.remove('hide');

    })
  ) : (
    PACIENTES.forEach(paciente => {
      paciente.classList.remove('hide');
    })
  )
});