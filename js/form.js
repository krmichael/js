"use strict";

const MSG_ERROR = document.querySelector('#mensagens-erro');

document.querySelector('.form').addEventListener('submit', (e) => {

  e.preventDefault();
  const FORM = e.target;

  let paciente = obtemPacienteDoFormulario(FORM);

  let erros = validaPaciente(paciente);
  
  if (erros.length > 0) {
    exibeMensagensErro(erros);

    return;
  }

  adicionaPacienteNatabela(paciente);

  FORM.reset();
  FORM.nome.focus();
  MSG_ERROR.innerHTML = ""; //Limpa todas as mensagens de erro ao submeter o form

});

function adicionaPacienteNatabela(paciente) {
  let tr = montaTr(paciente);
  document.querySelector('tbody').appendChild(tr);
}

function obtemPacienteDoFormulario(form) {

  let paciente = {
    nome: form.nome.value,
    peso: form.peso.value,
    altura: form.altura.value,
    gordura: form.gordura.value,
    imc: CALCULA_IMC(form.peso.value, form.altura.value)
  }

  return paciente;
}

function exibeMensagensErro(erros) {
  MSG_ERROR.innerHTML = ""; //Limpa as msg de erro quando o valor do campo é válido. 

  erros.forEach(erro => {
    let li = document.createElement('li');
    li.textContent = erro;
    MSG_ERROR.appendChild(li);
  });
}

function montaTr(paciente) {
  let tr = document.createElement('tr');
  tr.classList.add('paciente');

  tr.appendChild(montaTd(paciente.nome, 'nome'));
  tr.appendChild(montaTd(paciente.peso, 'peso'));
  tr.appendChild(montaTd(paciente.altura, 'altura'));
  tr.appendChild(montaTd(paciente.gordura, 'gordura'));
  tr.appendChild(montaTd(paciente.imc, 'imc'));

  return tr;
}

function montaTd(dado, classe) {
  let td = document.createElement('td');
  td.textContent = dado;
  td.classList.add(classe);

  return td;
}

function validaPaciente(paciente) {

  let erros = [];

  if (paciente.nome.length == 0) {
    erros.push("O campo nome não pode ser vazio!");
  }

  if (!VALIDA_PESO(paciente.peso) || paciente.peso.length == 0) {
    erros.push("Peso inválido!");
  }

  if (!VALIDA_ALTURA(paciente.altura) || paciente.altura.length == 0) {
    erros.push("Altura inválida!");
  }

  if (paciente.gordura.length == 0) {
    erros.push("O campo gordura não pode ser vazio!");
  }

  return erros;
}