"use strict";

document.querySelector('.table-paciente tbody').addEventListener('dblclick', (event) => {
  
  let parent = event.target.parentNode;
  parent.classList.add('fadeOut');
  
  setTimeout(() => parent.remove(), 500);

});